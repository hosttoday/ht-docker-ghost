# @hosttoday/ht-docker-elk
elastic stack with full basic authentication

## Availabililty and Links
* [npmjs.org (npm package)](https://www.npmjs.com/package/@hosttoday/ht-docker-node)
* [gitlab.com (source)](https://gitlab.com/hosttoday/ht-docker-elk)
* [github.com (source mirror)](https://github.com/hosttoday/ht-docker-elk)
* [docs (typedoc)](https://hosttoday.gitlab.io/ht-docker-elk/)

## Status for master
[![build status](https://gitlab.com/hosttoday/ht-docker-elk/badges/master/build.svg)](https://gitlab.com/hosttoday/ht-docker-elk/commits/master)
[![coverage report](https://gitlab.com/hosttoday/ht-docker-elk/badges/master/coverage.svg)](https://gitlab.com/hosttoday/ht-docker-elk/commits/master)
[![npm downloads per month](https://img.shields.io/npm/dm/@hosttoday/ht-docker-node.svg)](https://www.npmjs.com/package/@hosttoday/ht-docker-node)
[![Known Vulnerabilities](https://snyk.io/test/npm/@hosttoday/ht-docker-node/badge.svg)](https://snyk.io/test/npm/@hosttoday/ht-docker-node)
[![TypeScript](https://img.shields.io/badge/TypeScript->=%203.x-blue.svg)](https://nodejs.org/dist/latest-v10.x/docs/api/)
[![node](https://img.shields.io/badge/node->=%2010.x.x-blue.svg)](https://nodejs.org/dist/latest-v10.x/docs/api/)
[![JavaScript Style Guide](https://img.shields.io/badge/code%20style-prettier-ff69b4.svg)](https://prettier.io/)

## Usage

This image runs the elastic stack and supports full basic authentication

```
docker run --rm -d --name elstack -p 3000:3000 -p 9200:9201 -e SSL=true -e ELSK_USER="anyuser" -e ELSK_PASS="anypass" registry.gitlab.com/hosttoday/ht-docker-elk
```

## Contribute
 We are always happy for code contributions. If you are not the code contributing type that is ok. + Still, maintaining Open Source repositories takes considerable time and thought. If you like the quality of what we do and our modules are useful to you we would appreciate a little monthly contribution: [Contribute monthly :)](https://lossless.link/contribute)

For further information read the linked docs at the top of this readme.

> MIT licensed | **&copy;** [Lossless GmbH](https://lossless.gmbh)
| By using this npm module you agree to our [privacy policy](https://lossless.gmbH/privacy)

[![repo-footer](https://lossless.gitlab.io/publicrelations/repofooter.svg)](https://maintainedby.lossless.com)
